package main.com.conference.management;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import main.com.conference.management.vo.Charla;


/**
 * 
 * @author crist
 *
 */
public class administradorArchivosOrigenConferencia {

	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws FileNotFoundException
	 */
	 public List<Charla> buscarCharlas(String fileName) throws FileNotFoundException{
	        FileInputStream fstream = null;
	        List<Charla> charlaList = new ArrayList<>();

	        try {
	            fstream = new FileInputStream(fileName);
	        } catch (FileNotFoundException e) {
	            System.err.println("Archivo de entrada no encontrado : " + ConfiguracionGestionConferencias.ARCHIVO_ENTRADA_CHARLAS + ". Aseg�rese de que el archivo exista");
	            throw e;
	        }
	        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

	        String strLine;
	        int intMinutos;

	        // Leer archivo de entrada l�nea por l�nea
	        try {
	            while ((strLine = br.readLine()) != null) {
	                // Manejar comentarios o l�neas vac�as.
	                if(strLine.contains("//") || strLine.isEmpty())
	                    continue;

	                String titulo = strLine.substring(0, strLine.lastIndexOf(" "));
	                String minutosString = strLine.substring(strLine.lastIndexOf(" ") + 1);
	                // obtenga solo los n�meros enteros como l�nea de cadena.
	                String minutos = strLine.replaceAll("\\D+", "");
	                if (ConfiguracionGestionConferencias.LIGHTNING_CHARLA.equals(minutosString)) {
	                    intMinutos = ConfiguracionGestionConferencias.LIGHTNING_CHARLA_DURACION_MINUTOS;
	                } else {
	                    try {
	                        intMinutos = Integer.parseInt(minutos);
	                    } catch (NumberFormatException e) {
	                        System.err.println("No se pudo analizar la l�nea : " + strLine);
	                        throw e;
	                    }
	                }
	                Charla singleTalk = new Charla(titulo, intMinutos);
	                charlaList.add(singleTalk);

	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                fstream.close();
	                br.close();
	            } catch (IOException e){
	                System.err.println(e.getMessage());
	            }
	        }
	        return charlaList;
	    }

	    public List<Charla> buscarCharlas() throws FileNotFoundException{
	        return buscarCharlas(ConfiguracionGestionConferencias.ARCHIVO_ENTRADA_CHARLAS);
	    }
}
