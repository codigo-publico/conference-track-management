package main.com.conference.management;

import java.text.SimpleDateFormat;
import java.util.List;

import main.com.conference.management.vo.Conferencia;
import main.com.conference.management.vo.Evento;
import main.com.conference.management.vo.Slot;
import main.com.conference.management.vo.Track;

/**
 * 
 * @author crist
 *
 */
public class AdministrarSalidaConsola {

	public void imprimirHorario (Conferencia conferencia) {

        SimpleDateFormat sdf = ConfiguracionGestionConferencias.FORMATO_FECHA;
        System.out.println("Resultado: Calendario de conferencias :");
        System.out.println("--------------------------------------------------------");
        for(Track track : conferencia.getTracks()){
            System.out.println("Track " + track.getTrackId());
            List<Slot> slots = track.getSlots();
            System.out.println("");

            // Genera las conversaciones en pistas seg�n el total de conversaciones y el recuento de conversaciones.
            for (Slot slot : slots) {

                for (Evento event : slot.getEventos()) {
                    // Imprime el t�tulo de la charla preparada para este Track.
                    System.out.println(sdf.format(event.getHoraInicio().getTime()) + " " + event.getTitulo()
                            + " " + event.getDuracionEnMinutos());
                }
            }
            System.out.println("--------------------------------------------------------");
        }
    }
}
