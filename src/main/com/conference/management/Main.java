package main.com.conference.management;

import java.util.List;

import main.com.conference.management.util.ConferenciaUtils;
import main.com.conference.management.vo.Conferencia;
import main.com.conference.management.vo.Charla;

/**
 * 
 * @author crist
 *
 */
public class Main {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		AdministrarConferencia administrarConferencia = new AdministrarConferencia();
        List<Charla> charlaList = null;
        // Obtenga la lista de conversaciones de entrada.
        try {
            charlaList = administrarConferencia.fetchTalksListFromSource(ConfiguracionGestionConferencias.ARCHIVO_ORIGEN_CHEALAS);
        } catch (Exception e){
            System.err.println(e.toString());
        }

        if(charlaList == null || charlaList.size() == 0)
            return;

        // Mostrar charlas
        ConferenciaUtils.printCharlas(charlaList);

        // Procese y programe charlas en eventos y espacios.
        Conferencia conferencia = administrarConferencia.procesarAndProgramarCharlas(charlaList);

        // Salida de los eventos de la conferencia.
        try {
				administrarConferencia.salidaCalendarioConferencias(conferencia, ConfiguracionGestionConferencias.SALIDA_DATOS_CONSOLA);			
        } catch (Exception e){
            System.err.println(e.toString());
        }
	}

}
