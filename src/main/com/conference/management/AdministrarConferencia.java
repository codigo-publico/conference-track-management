package main.com.conference.management;

import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import main.com.conference.management.util.ConferenciaUtils;
import main.com.conference.management.vo.Conferencia;
import main.com.conference.management.vo.Evento;
import main.com.conference.management.vo.Lunch;
import main.com.conference.management.vo.Networking;
import main.com.conference.management.vo.Slot;
import main.com.conference.management.vo.Charla;
import main.com.conference.management.vo.CompararCharlas;
import main.com.conference.management.vo.Track;

/**
 * 
 * @author crist
 *
 */
public class AdministrarConferencia {

	 public List<Charla> fetchTalksListFromSource(String fuenteCharlaFile) throws Exception{

	        // Esto crear� una instancia del SourceManager requerido.
	        if (fuenteCharlaFile.equals(ConfiguracionGestionConferencias.ARCHIVO_ORIGEN_CHEALAS)) {
	            administradorArchivosOrigenConferencia origenAdministrador = new administradorArchivosOrigenConferencia();
	            try {
	                return origenAdministrador.buscarCharlas();
	            } catch (FileNotFoundException e) {

	                return null;
	            }
	        } else {
	            throw new Exception("Tipo de fuente no v�lida");
	        }
	    }

	    public void salidaCalendarioConferencias(Conferencia conferencia, String salidaDatoConsola) throws Exception {

	        // Esto crear� una instancia del OutputManager requerido.
	        if (salidaDatoConsola.equals(ConfiguracionGestionConferencias.SALIDA_DATOS_CONSOLA)) {
	            AdministrarSalidaConsola outputManager = new AdministrarSalidaConsola();
	            outputManager.imprimirHorario(conferencia);
	        } else {
	            throw new Exception("Destino no v�lida.");
	        }
	    }


	    public Conferencia procesarAndProgramarCharlas(List<Charla> charlaList){
	        Conferencia conferencia = new Conferencia();

	        // ordenar todas las conversaciones en orden descendente
	        Collections.sort(charlaList, new CompararCharlas());
	        int trackCount = 0;

	        // se ejecuta este bucle hasta que todas las conversaciones est�n programadas.
	        while (0 != charlaList.size()) {

	            // crear y llenar el espacio de la ma�ana.
	            Slot morningSlot = new Slot(ConfiguracionGestionConferencias.MA�ANA_SLOT_DURACION_MINUTOS, ConfiguracionGestionConferencias.HORA_INICIO_CHARLA);
	            fillSlot(morningSlot, charlaList);

	            // crear y llenar el espacio para el almuerzo.
	            Slot lunchSlot = new Slot(ConfiguracionGestionConferencias.LUNCH_DURACION_MINUTOS, ConfiguracionGestionConferencias.HORA_INICIO_LUNCH);
	            lunchSlot.addEvent(new Lunch());

	            // crear y llenar el espacio de la tarde.
	            Slot afternoonSlot = new Slot(ConfiguracionGestionConferencias.TARDE_SLOT_DURACION_MINUTOS,
	                    ConfiguracionGestionConferencias.HORA_INICIO_RANUR_DESPUES_LUNCH);
	            fillSlot(afternoonSlot, charlaList);

	            // crear y llenar el espacio de networking.
	            Slot networkingSlot = new Slot(ConfiguracionGestionConferencias.NETWORKING_DURACION_MINUTOS,
	                    ConfiguracionGestionConferencias.HORA_INICIO_NETWORKING);
	            networkingSlot.addEvent(new Networking());

	            // agregue todos los espacios del d�a a la pista "track".
	            Track track = new Track(++trackCount);
	            track.addSlot(morningSlot);
	            track.addSlot(lunchSlot);
	            track.addSlot(afternoonSlot);
	            track.addSlot(networkingSlot);
	            // agregar pista "track" a la conferencia.
	            conferencia.addTrack(track);
	        }

	        return conferencia;
	    }

	    private void fillSlot(Slot slot, List<Charla> charlas) {
	        // inicializar la hora de inicio de la ranura.
	        Calendar currentStartTime = slot.getHoraInicio();
	        for (Iterator<Charla> talksIterator = charlas.iterator(); talksIterator.hasNext();) {
	            Charla charla = talksIterator.next();
	            if (slot.hasRoomFor(charla)) {
	                // agregue un evento a la ranura en el currentStartTime calculado.
	                slot.addEvent(new Evento(currentStartTime, charla.getTitulo(), charla.getDurationInMinutes()));
	                // calcule la pr�xima hora de inicio en funci�n de la hora de inicio actual y la duraci�n de la conversaci�n actual.
	                currentStartTime = ConferenciaUtils.getHoraInicioSiguiente(currentStartTime, charla);
	                // eliminar la charla de la lista. Esto significa que la charla ha sido programada en la conferencia.
	                talksIterator.remove();
	            }
	        }
	    }
}
