package main.com.conference.management.util;

import java.util.Calendar;
import java.util.List;

import main.com.conference.management.vo.Charla;


/**
 * 
 * @author crist
 *
 */
public class ConferenciaUtils {

	/**
	 * 
	 * @param horas
	 * @param minutos
	 * @return
	 */
	public static Calendar getTiempoCalendario(int horas, int minutos) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, horas);
        cal.set(Calendar.MINUTE, minutos);
        return cal;
    }

	/**
	 * 
	 * @param horaInicioActual
	 * @param charlaActual
	 * @return
	 */
    public static Calendar getHoraInicioSiguiente(Calendar horaInicioActual, Charla charlaActual) {
        Calendar newTime = Calendar.getInstance();
        newTime.set(Calendar.HOUR_OF_DAY, horaInicioActual.get(Calendar.HOUR_OF_DAY));
        newTime.set(Calendar.MINUTE, horaInicioActual.get(Calendar.MINUTE));
        newTime.add(Calendar.MINUTE, charlaActual.getDurationInMinutes());
        return newTime;
    }

    /**
     * 
     * @param charlaList
     */
    public static void printCharlas(List<Charla> charlaList) {
        System.out.println("--------------------------------------------------------");
        System.out.println("Entrada de chalas:");
        for (Charla charla : charlaList) {
            // Imprime el t�tulo de la charla preparada para este Track.
            System.out.println(charla.getTitulo() + " " + charla.getDurationInMinutes());
        }
        System.out.println("--------------------------------------------------------");
    }
}
