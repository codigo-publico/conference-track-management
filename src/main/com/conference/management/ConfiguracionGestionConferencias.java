package main.com.conference.management;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import main.com.conference.management.util.ConferenciaUtils;

/**
 * 
 * @author crist
 *
 */
public class ConfiguracionGestionConferencias {

	public static final String ARCHIVO_ENTRADA_CHARLAS = "entrada-charlas.txt";
    public static final SimpleDateFormat FORMATO_FECHA = new SimpleDateFormat("hh:mm a");

    public static final String LIGHTNING_CHARLA = "lightning";

    public static final int TOTAL_TRACK_DURACION_MINUTOS = 420;
    public static final int MA�ANA_SLOT_DURACION_MINUTOS = 180;
    public static final int TARDE_SLOT_DURACION_MINUTOS = 240;

    public static final int LIGHTNING_CHARLA_DURACION_MINUTOS = 5;

    public static Calendar HORA_INICIO_CHARLA = ConferenciaUtils.getTiempoCalendario(9, 0);;
    public static Calendar HORA_INICIO_LUNCH = ConferenciaUtils.getTiempoCalendario(12, 0);
    public static Calendar HORA_INICIO_RANUR_DESPUES_LUNCH = ConferenciaUtils.getTiempoCalendario(13, 0);
    public static Calendar HORA_INICIO_NETWORKING = ConferenciaUtils.getTiempoCalendario(17, 0);

    public static final int LUNCH_DURACION_MINUTOS = 60;
    public static final int NETWORKING_DURACION_MINUTOS = 60;
    
    public static final String ARCHIVO_ORIGEN_CHEALAS = "FILE";
    public static final String SALIDA_DATOS_CONSOLA = "CONSOLE";
    
}
