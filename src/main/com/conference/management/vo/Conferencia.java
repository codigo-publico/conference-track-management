package main.com.conference.management.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author crist
 *
 */
public class Conferencia {

	List<Track> tracks;

    public List<Track> getTracks() {
        return tracks;
    }

    public void addTrack(Track track) {
        this.tracks.add(track);
    }

    public Conferencia(){
        this.tracks = new ArrayList();
    }
}
