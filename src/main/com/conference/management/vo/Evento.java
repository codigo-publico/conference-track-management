package main.com.conference.management.vo;

import java.util.Calendar;

/**
 * 
 * @author crist
 *
 */
public class Evento {

	private Calendar horaInicio;
    private int duracionMinutos;
    private String titulo;

    public Evento(Calendar horaInicio, String titulo, int duracionMinutos){
        this.horaInicio = horaInicio;
        this.titulo = titulo;
        this.duracionMinutos = duracionMinutos;
    }

    public Calendar getHoraInicio() {
        return horaInicio;
    }

    public int getDuracionEnMinutos() {
        return duracionMinutos;
    }

    public String getTitulo() {
        return titulo;
    }
}
