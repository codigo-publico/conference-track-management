package main.com.conference.management.vo;


/**
 * 
 * @author crist
 *
 */
public class Charla {
	
	 private int durationInMinutes;
	    private String titulo;

	    public int getDurationInMinutes() {
	        return durationInMinutes;
	    }

	    public String getTitulo() {
	        return titulo;
	    }

	    public Charla(String titulo, int durationInMinutes)
	    {
	        this.durationInMinutes = durationInMinutes;
	        this.titulo = titulo;
	    }
}
