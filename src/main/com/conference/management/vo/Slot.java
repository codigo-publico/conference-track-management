package main.com.conference.management.vo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 
 * @author crist
 *
 */
public class Slot {

	private List<Evento> eventos;
    private int duracionRestante;
    private Calendar horaInicio;

    public List<Evento> getEventos() {
        return eventos;
    }

    public void addEvent(Evento evento) {
        this.eventos.add(evento);
        this.duracionRestante -= evento.getDuracionEnMinutos();
    }

    public Calendar getHoraInicio() {
        return horaInicio;
    }

    public Slot(int duracion, Calendar horaInicio){
        eventos = new ArrayList<>();
        this.duracionRestante = duracion;
        this.horaInicio = horaInicio;
    }

    // compruebe si la charla se puede acomodar en el horario actual.
    public boolean hasRoomFor(Charla charla) {
        return duracionRestante >= charla.getDurationInMinutes();
    }
}
