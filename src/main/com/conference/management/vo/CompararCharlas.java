package main.com.conference.management.vo;

import java.util.Comparator;

/**
 * 
 * @author crist
 *
 */
public class CompararCharlas implements Comparator<Charla>{

    @Override
    public int compare(Charla t1, Charla t2) {
        if(t1.getDurationInMinutes() < t2.getDurationInMinutes()){
            return 1;
        } else {
            return -1;
        }
    }
}
