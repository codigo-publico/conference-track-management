package main.com.conference.management.vo;

import main.com.conference.management.ConfiguracionGestionConferencias;

/**
 * 
 * @author crist
 *
 */
public class Networking extends Evento {

    public Networking () {
        super(ConfiguracionGestionConferencias.HORA_INICIO_NETWORKING, "Networking Event", 0);
    }

}
