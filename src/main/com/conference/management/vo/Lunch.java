package main.com.conference.management.vo;

import main.com.conference.management.ConfiguracionGestionConferencias;

/**
 * 
 * @author crist
 *
 */
public class Lunch extends Evento {
    public Lunch() {
        super(ConfiguracionGestionConferencias.HORA_INICIO_LUNCH, "Lunch", ConfiguracionGestionConferencias.LUNCH_DURACION_MINUTOS);
    }

}
