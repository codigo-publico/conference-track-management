package test.com.conference.management;


import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import main.com.conference.management.util.ConferenciaUtils;
import main.com.conference.management.vo.Charla;

public class ConferenciaUtilsTest {

	@Test
	public void testGetCalendarTime() {

        Calendar cal1 = ConferenciaUtils.getTiempoCalendario(5, 50);

        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 5);
        cal2.set(Calendar.MINUTE, 50);
        Assert.assertEquals(cal1.get(Calendar.HOUR_OF_DAY), cal2.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals(cal1.get(Calendar.MINUTE), cal2.get(Calendar.MINUTE));

        cal1 = ConferenciaUtils.getTiempoCalendario(5, 70);
        cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 5);
        cal2.set(Calendar.MINUTE, 70);

        Assert.assertEquals(cal1.get(Calendar.HOUR_OF_DAY), cal2.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals(cal1.get(Calendar.MINUTE), cal2.get(Calendar.MINUTE));

    }

		
	 @Test
	 public void testGetNextStartTime() {
	  
		 Calendar currentStartTime = ConferenciaUtils.getTiempoCalendario(5, 50);
		 Charla talk = new Charla("Test Charla", 20);

	        Calendar horaInicioSiguineteManual = ConferenciaUtils.getTiempoCalendario(6, 10);
	        Calendar calcularHoraInicioSiguiente = ConferenciaUtils.getHoraInicioSiguiente(currentStartTime, talk);

	        Assert.assertEquals(horaInicioSiguineteManual.get(Calendar.HOUR_OF_DAY), calcularHoraInicioSiguiente.get(Calendar.HOUR_OF_DAY));
	        Assert.assertEquals(horaInicioSiguineteManual.get(Calendar.MINUTE), calcularHoraInicioSiguiente.get(Calendar.MINUTE));
	    }
}


