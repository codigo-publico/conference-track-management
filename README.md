# GESTI�N DE SEGUIMIENTO DE CONFERENCIAS 
##Problema: gesti�n de pistas de conferencia.

Est�s planeando una gran conferencia de programaci�n y has recibido muchas propuestas que han superado la pantalla inicial, pero tienes problemas para ajustarlas a las restricciones de tiempo del d�a, �hay tantas posibilidades! As� que escribes un programa para que lo haga por ti.

� La conferencia tiene m�ltiples pistas, cada una con una sesi�n de ma�ana y otra de tarde.

� Cada sesi�n contiene varias charlas.

� Las sesiones de la ma�ana comienzan a las 9 am y deben finalizar antes del mediod�a, para el almuerzo.

� Las sesiones de la tarde comienzan a la 1 pm y deben finalizar a tiempo para el evento de networking.

� El evento de networking puede comenzar no antes de las 4:00 y no m�s tarde de las 5:00.

� Ning�n t�tulo de charla tiene n�meros.

� Todas las duraciones de charlas son en minutos (no en horas) o rel�mpago (5 minutos).

� Los presentadores ser�n muy puntuales; no debe haber brecha entre las sesiones.


Ten en cuenta que, dependiendo de c�mo elijas completar este problema, tu soluci�n puede proporcionar un orden o combinaci�n diferente de charlas en las pistas. Esto es aceptable; no necesitas duplicar exactamente la salida de ejemplo proporcionada aqu�.


# Soluci�n

## Instrucciones de uso

### Tecnolog�a utilizada:
	* Windows 11 Home
	* eclipse 2021-12-R
	* java version "1.8.0_251"
	
### Primer uso:
	* Para ejecutar el programa necesita ejecutar se requiere seguir los siguientes pasos:
		1.	Descargar el proyecto
		2.	Abrir ventana de comando �CMD�
		3.	Dentro de la ventana comando ir a la ruta /ruta-descarga/ conference-track-management/ejecutable
		4.	En la carpeta ejecutable se encontrar� 2 archivos conference-track-management.jar y entrada-charlas.txt en este �ltimo es la entrada de datos de las conferencias.
		5.	 Ejecutar el comando �java -jar conference-track-management.jar�

	
	



# conference-track-management
ThoughtWorks coding problem: Conference Track Management

You are planning a big programming conference and have received many proposals which have passed the initial screen process but you're having trouble fitting them into the time constraints of the day -- there are so many possibilities! So you write a program to do it for you.

*The conference has multiple tracks each of which has a morning and afternoon session.

*Each session contains multiple talks.

*Morning sessions begin at 9am and must finish by 12 noon, for lunch.

*Afternoon sessions begin at 1pm and must finish in time for the networking event.

*The networking event can start no earlier than 4:00 and no later than 5:00.

*No talk title has numbers in it.

*All talk lengths are either in minutes (not hours) or lightning (5 minutes).

*Presenters will be very punctual; there needs to be no gap between sessions.

Note that depending on how you choose to complete this problem, your solution may give a different ordering or combination of talks into tracks. This is acceptable; you don�t need to exactly duplicate the sample output given here.
 

### Test input:
```
Writing Fast Tests Against Enterprise Rails 60min
Overdoing it in Python 45min
Lua for the Masses 30min
Ruby Errors from Mismatched Gem Versions 45min
Common Ruby Errors 45min
Rails for Python Developers lightning
Communicating Over Distance 60min
Accounting-Driven Development 45min
Woah 30min
Sit Down and Write 30min
Pair Programming vs Noise 45min
Rails Magic 60min
Ruby on Rails: Why We Should Move On 60min
Clojure Ate Scala (on my project) 45min
Programming in the Boondocks of Seattle 30min
Ruby vs. Clojure for Back-End Development 30min
Ruby on Rails Legacy App Maintenance 60min
A World Without HackerNews 30min
User Interface CSS in Rails Apps 30min
```


### Test output: 

```
Track 1:
09:00AM Writing Fast Tests Against Enterprise Rails 60min
10:00AM Overdoing it in Python 45min
10:45AM Lua for the Masses 30min
11:15AM Ruby Errors from Mismatched Gem Versions 45min
12:00PM Lunch
01:00PM Ruby on Rails: Why We Should Move On 60min
02:00PM Common Ruby Errors 45min
02:45PM Pair Programming vs Noise 45min
03:30PM Programming in the Boondocks of Seattle 30min
04:00PM Ruby vs. Clojure for Back-End Development 30min
04:30PM User Interface CSS in Rails Apps 30min
05:00PM Networking Event

Track 2:
09:00AM Communicating Over Distance 60min
10:00AM Rails Magic 60min
11:00AM Woah 30min
11:30AM Sit Down and Write 30min
12:00PM Lunch
01:00PM Accounting-Driven Development 45min
01:45PM Clojure Ate Scala (on my project) 45min
02:30PM A World Without HackerNews 30min
03:00PM Ruby on Rails Legacy App Maintenance 60min
04:00PM Rails for Python Developers lightning
05:00PM Networking Event